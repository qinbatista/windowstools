
// WindowsToolsDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WindowsTools.h"
#include "WindowsToolsDlg.h"
#include "afxdialogex.h"
#include <Windows.h>
#include <tlhelp32.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define MOUSEMOVETIMER_X 8
#define MOUSEMOVETIMER_Y 9
#define AUTOCUT 10
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CWindowsToolsDlg 对话框



CWindowsToolsDlg::CWindowsToolsDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_WINDOWSTOOLS_DIALOG, pParent)
	, m_isAutoCreateOn(false)
	, m_loop(0)
	, m_hwndcount(0)
	, m_iswindowhide(false)
	, m_istoolopen(true)
	, m_isdota(false)
	, m_isminilevel(false)
	, m_savepoint_x(0)
	, m_savepoint_y(0)
	, ExportJar(0)
	, m_xx(0)
	, m_yy(0)

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWindowsToolsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CWindowsToolsDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CWindowsToolsDlg::OnBnClickedButton1)
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CWindowsToolsDlg::OnBnClickedOk)
	//ON_BN_CLICKED(IDC_BUTTON2, &CWindowsToolsDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CWindowsToolsDlg::OnBnClickedButton3)
	ON_STN_CLICKED(IDC_x5, &CWindowsToolsDlg::OnStnClickedx5)
END_MESSAGE_MAP()


// CWindowsToolsDlg 消息处理程序

HHOOK g_ms_hook = 0;
HHOOK g_kb_hook = 0;
LRESULT CALLBACK kb_proc(int code, WPARAM w, LPARAM l)
{ 

	if (w == WM_KEYDOWN)
	{
		KBDLLHOOKSTRUCT* pSturct = (KBDLLHOOKSTRUCT*)l;
		CWnd *pWnd = AfxGetApp()->GetMainWnd();
		CWindowsToolsDlg *pDlg = (CWindowsToolsDlg*)pWnd;

		if (pSturct->vkCode== VK_NUMPAD1)
		{
			pDlg->ShowDialog();//UnhookWindowsHookEx(g_kb_hook); UnhookWindowsHookEx(g_ms_hook); break;
		}
		if (pSturct->vkCode == 110 && GetKeyState(96) & 0x8000)//先按F6再按F5Y
		{
			pDlg->m_istoolopen = !pDlg->m_istoolopen;
			if (pDlg->m_istoolopen == true)
			{
				pDlg->GetDlgItem(IDC_x3)->SetWindowText(_T("快捷键开启"));
			}
			else
			{
				pDlg->GetDlgItem(IDC_x3)->SetWindowText(_T("快捷键关闭"));
			}
		}

		if (pDlg->m_istoolopen == false)
		{
			return CallNextHookEx(g_kb_hook, code, w, l);
		}
		switch (pSturct->vkCode)
		{
			case VK_NUMPAD2:pDlg->OnBnClickedOk();; break;//自动输入hello123456
			case VK_NUMPAD3:pDlg->InputUnity(); break;//SetCursorPos(10, 10); break;			
			case VK_NUMPAD4:pDlg->InputGit(); break;
			case VK_NUMPAD5:pDlg->AntoCreate(); break;
			case VK_NUMPAD6:pDlg->InputDota(); break;
			case VK_NUMPAD7:pDlg->InputMiniLevel(); break;
			case VK_F11:pDlg->CreateSetMouse(); break;
			case VK_F12:pDlg->CreateJar(); break;
		}
		if (pSturct->vkCode== VK_F5 && GetKeyState(VK_F6) & 0x8000)//先按F6再按F5
		{
			SetCursorPos(10, 10);
		}
	}


	return CallNextHookEx(g_kb_hook, code, w, l);
}
LRESULT CALLBACK ms_proc(int code, WPARAM w, LPARAM l)
{
	KBDLLHOOKSTRUCT* pSturct = (KBDLLHOOKSTRUCT*)l;
	CWnd *pWnd = AfxGetApp()->GetMainWnd();
	CWindowsToolsDlg *pDlg = (CWindowsToolsDlg*)pWnd;
	if (w == WM_MOUSEMOVE)
	{
		pDlg->StopAuto();

	}

	return CallNextHookEx(g_ms_hook, code, w, l);
}
BOOL CWindowsToolsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	SetTimer(1, 10, NULL);//时间钟1
	SetTimer(2, 1, NULL);//时间钟2

	SetTimer(8, 1000, NULL);//时间钟1
	g_kb_hook = SetWindowsHookEx(WH_KEYBOARD_LL, kb_proc, GetModuleHandle(NULL), 0);
	g_ms_hook = SetWindowsHookEx(WH_MOUSE_LL, ms_proc, GetModuleHandle(NULL), 0);

	//不在工具栏显示
	ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);
	//置顶
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	GetDlgItem(IDC_x3)->SetWindowText(_T("快捷键开启"));
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CWindowsToolsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWindowsToolsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWindowsToolsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CWindowsToolsDlg::OnBnClickedButton1()
{
	HKEY hKey;
	//找到系统的启动项 
	LPCTSTR lpRun = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
	//打开启动项Key 
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE, &hKey);
	if (lRet == ERROR_SUCCESS)
	{
		char pFileName[MAX_PATH] = { 0 };
		//得到程序自身的全路径 
		DWORD dwRet = GetModuleFileName(NULL, (LPWSTR)pFileName, MAX_PATH);
		//添加一个子Key,并设置值 // 下面的"test"是应用程序名字（不加后缀.exe）
		lRet = RegSetValueEx(hKey, _T("WindowsTools"), 0, REG_SZ, (BYTE *)pFileName, dwRet);
		//关闭注册表 
		RegCloseKey(hKey);
		if (lRet != ERROR_SUCCESS)
		{
			MessageBox(_T("系统参数错误,不能完成开机启动设置"));
		}
		else
		{
			MessageBox(_T("打开开机启动成功"));
		}
		//isrun = 1;
	}


}
void CWindowsToolsDlg::SendUnicode(wchar_t data)
{
	INPUT input[2];
	memset(input, 0, 2 * sizeof(INPUT));

	input[0].type = INPUT_KEYBOARD;
	input[0].ki.wVk = 0;
	input[0].ki.wScan = data;
	input[0].ki.dwFlags = 0x4;//KEYEVENTF_UNICODE;

	input[1].type = INPUT_KEYBOARD;
	input[1].ki.wVk = 0;
	input[1].ki.wScan = data;
	input[1].ki.dwFlags = KEYEVENTF_KEYUP | 0x4;//KEYEVENTF_UNICODE;

	SendInput(2, input, sizeof(INPUT));
}
void CWindowsToolsDlg::SendKeys(CString msg)
{
	short vk;
	BOOL shift;
	USES_CONVERSION;
	wchar_t* data = T2W(msg.GetBuffer(0));
	int len = wcslen(data);
	for (int i = 0; i<len; i++)
	{
		if (data[i] >= 0 && data[i]<256) //ascii字符
		{
			vk = VkKeyScanW(data[i]);
			if (vk == -1)
			{
				SendUnicode(data[i]);
			}
			else
			{
				if (vk < 0)
				{
					vk = ~vk + 0x1;
				}

				shift = vk >> 8 & 0x1;

				if (GetKeyState(VK_CAPITAL) & 0x1)
				{
					if (data[i] >= 'a' && data[i] <= 'z' || data[i] >= 'A' && data[i] <= 'Z')
					{
						shift = !shift;
					}
				}
				SendAscii(vk & 0xFF, shift);
			}
		}
		else //unicode字符
		{
			SendUnicode(data[i]);
		}
	}
}
void CWindowsToolsDlg::SendAscii(wchar_t data, BOOL shift)
{
	INPUT input[2];
	memset(input, 0, 2 * sizeof(INPUT));

	if (shift)
	{
		input[0].type = INPUT_KEYBOARD;
		input[0].ki.wVk = VK_SHIFT;
		SendInput(1, input, sizeof(INPUT));
	}
	input[0].type = INPUT_KEYBOARD;
	input[0].ki.wVk = data;
	input[1].type = INPUT_KEYBOARD;
	input[1].ki.wVk = data;
	input[1].ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(2, input, sizeof(INPUT));
	if (shift)
	{
		input[0].type = INPUT_KEYBOARD;
		input[0].ki.wVk = VK_SHIFT;
		input[0].ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, input, sizeof(INPUT));
	}
}
BOOL CWindowsToolsDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (pMsg->wParam == VK_F2 && pMsg->wParam == VK_F3)
	{
		// TODO: 在此添加控件通知处理程序代码
		         
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CWindowsToolsDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	CDialogEx::OnMouseMove(nFlags, point); 
}

int looppoint=0,realyx,realyy;
int counttime = 0;
void CWindowsToolsDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == 1)
	{
		POINT point;
		GetCursorPos(&point);
		CString strX, strY;
		strX.Format(_T("%d"), point.x);
		strY.Format(_T("%d"), point.y);
		GetDlgItem(IDC_x)->SetWindowText(strX);
		GetDlgItem(IDC_Y)->SetWindowText(strY);
	}
	if (nIDEvent == 2)
	{
		this->ShowWindow(SW_HIDE);
		KillTimer(2);
	}
	if (nIDEvent == 3)
	{
		keybd_event(VK_SPACE, 0, 0, 0);
		mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
		//mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
		m_loop++;
		m_loop = m_loop % 3;
		keybd_event(55 + m_loop, 0, 0, 0);
		keybd_event(55 + m_loop, 0, KEYEVENTF_KEYUP, 0);
	}
	if (nIDEvent == 4)
	{
		SetCursorPos(rand() % 1200, rand() % 800);
		mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
	}
	if (nIDEvent == 5)
	{
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
	}
	if (nIDEvent == 6)
	{
		keybd_event(50, 0, 0, 0);
		keybd_event(50, 0, KEYEVENTF_KEYUP, 0);
		KillTimer(5);
		mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
		SetTimer(7, 4000, NULL);
	}
	if (nIDEvent == 7)
	{
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
		keybd_event(49, 0, 0, 0);
		keybd_event(49, 0, KEYEVENTF_KEYUP, 0);
		SetTimer(5, 100, NULL);
	}

	//if (nIDEvent == MOUSEMOVETIMER_X)
	//{
	//	if (looppoint % 20 <= 200)
	//	{
	//		realyx = m_savepoint_x + (looppoint++) % 30;
	//	}
	//	else
	//	{
	//		realyx = m_savepoint_x - (looppoint++) % 20;
	//	}
	//	SetCursorPos(realyx, realyy);
	//}
	//if (nIDEvent == MOUSEMOVETIMER_Y)
	//{

	//	if (looppoint % 20 <= 200)
	//	{
	//		//realyy = m_savepoint_y + (looppoint++) % 200;
	//	}
	//	else
	//	{
	//		//realyy = m_savepoint_y - (looppoint++) % 200;
	//	}
	//	SetCursorPos(realyx, realyy);

	//}

	CDialogEx::OnTimer(nIDEvent);
}


void CWindowsToolsDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码

		CString strUserName = _T("hello123456");
		//CString strUserPwd = _T("123456i");
		CWnd *cWnd;
		HWND hWnd;
		cWnd = GetForegroundWindow();
		hWnd = cWnd->m_hWnd;
		if (hWnd == this->m_hWnd)
		{
			OutputDebugString(L"程序本身窗口！");
		}

		// Get Target Thread ID and Attach Thread Input  
		DWORD ProcID;
		DWORD ThreadID = GetWindowThreadProcessId(hWnd, &ProcID);
		AttachThreadInput(GetCurrentThreadId(), ThreadID, TRUE);

		// Get Target Window
		hWnd = GetFocus()->m_hWnd;
		// Detach Thread Input  
		AttachThreadInput(GetCurrentThreadId(), ThreadID, FALSE);

		SendKeys(strUserName); //发送用户名到光标所在编辑框
							   //keybd_event(VK_RETURN, 0, 0, 0);  //按下Tab键使光标跳到下一编辑框                        
							   //keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
							   //SendKeys(strUserPwd); //发送用户密码到光标所在编辑框   
	//CDialogEx::OnOK();
}





void CWindowsToolsDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
		HKEY hKey;
	//找到系统的启动项 
	LPCTSTR lpRun = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
	//打开启动项Key 
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE, &hKey);
	if (lRet == ERROR_SUCCESS)
	{
		char pFileName[MAX_PATH] = { 0 };
		//得到程序自身的全路径 
		DWORD dwRet = GetModuleFileName(NULL, (LPWSTR)pFileName, MAX_PATH);
		//添加一个子Key,并设置值 // 下面的"test"是应用程序名字（不加后缀.exe）
		lRet = RegDeleteValue(hKey, _T("WindowsTools"));
		//关闭注册表 
		RegCloseKey(hKey);
		if (lRet != ERROR_SUCCESS)
		{
			MessageBox(_T("系统参数错误,不能完成取消开机启动设置"));
		}
		else {
			MessageBox(_T("关闭开机启动成功"));
		}

	}
}


void CWindowsToolsDlg::GetHandle()
{
	CWnd *cWnd;
	HWND hWnd;
	cWnd = GetForegroundWindow();
	hWnd = cWnd->m_hWnd;
	//if (hWnd == this->m_hWnd)
	//{
	//	OutputDebugString(L"程序本身窗口！");
	//}
	//CString str = (CString)hWnd;	
}


void CWindowsToolsDlg::AntoCreate()
{
	if (m_isAutoCreateOn == false)
	{
		SetTimer(3, 400, NULL);
		m_isAutoCreateOn = true;
	}
	else
	{
		keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
		m_isAutoCreateOn = false;
		KillTimer(3);
	}
}


void CWindowsToolsDlg::OnBnClickedButton3()
{
	PROCESSENTRY32 pe32 = { 0 };
	pe32.dwSize = sizeof(PROCESSENTRY32);
	HANDLE hProcess = 0;
	DWORD dwExitCode = 0;
	BOOLEAN bEnabled;

	HANDLE hProessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProessSnap != INVALID_HANDLE_VALUE)
	{
		if (::Process32First(hProessSnap, &pe32))
		{
			do
			{
				if (strcmp(CW2A(pe32.szExeFile),"StudentMain.exe") == 0)
				{
					hProcess = ::OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_CREATE_THREAD, FALSE, pe32.th32ProcessID);
					CloseHandle(hProessSnap);
					break;
				}
			} while (::Process32Next(hProessSnap, &pe32));
		}
	}

	LPVOID Param = VirtualAllocEx(hProcess, NULL, sizeof(DWORD), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	WriteProcessMemory(hProcess, Param, (LPVOID)&dwExitCode, sizeof(DWORD), NULL);

	HANDLE hThread = CreateRemoteThread(hProcess,
		NULL,
		NULL,
		(LPTHREAD_START_ROUTINE)ExitProcess,
		Param,
		NULL,
		NULL);
}


void CWindowsToolsDlg::InputGit()
{
	CString strUserName = _T("tool@east2west.cn");
	CString strUserPwd = _T("hello123456");
	CWnd *cWnd;
	HWND hWnd;
	cWnd = GetForegroundWindow();
	hWnd = cWnd->m_hWnd;
	if (hWnd == this->m_hWnd)
	{
		OutputDebugString(L"程序本身窗口！");
	}

	// Get Target Thread ID and Attach Thread Input  
	DWORD ProcID;
	DWORD ThreadID = GetWindowThreadProcessId(hWnd, &ProcID);
	AttachThreadInput(GetCurrentThreadId(), ThreadID, TRUE);

	// Get Target Window
	hWnd = GetFocus()->m_hWnd;
	// Detach Thread Input  
	AttachThreadInput(GetCurrentThreadId(), ThreadID, FALSE);

	SendKeys(strUserName); //发送用户名到光标所在编辑框
	keybd_event(VK_RETURN, 0, 0, 0);  //按下Tab键使光标跳到下一编辑框                        
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
	Sleep(400);
	SendKeys(strUserPwd); //发送用户密码到光标所在编辑框   
	keybd_event(VK_RETURN, 0, 0, 0);  //按下Tab键使光标跳到下一编辑框                        
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
}


void CWindowsToolsDlg::InputUnity()
{
	//GetDlgItem(IDC_x3)->SetWindowText(_T("F6"));
	CString strUserName = _T("hello123456");
	CString strUserPwd = _T("hello123456");
	CWnd *cWnd;
	HWND hWnd;
	cWnd = GetForegroundWindow();
	hWnd = cWnd->m_hWnd;
	if (hWnd == this->m_hWnd)
	{
		OutputDebugString(L"程序本身窗口！");
	}

	// Get Target Thread ID and Attach Thread Input  
	DWORD ProcID;
	DWORD ThreadID = GetWindowThreadProcessId(hWnd, &ProcID);
	AttachThreadInput(GetCurrentThreadId(), ThreadID, TRUE);

	// Get Target Window
	hWnd = GetFocus()->m_hWnd;
	// Detach Thread Input  
	AttachThreadInput(GetCurrentThreadId(), ThreadID, FALSE);

	SendKeys(strUserName); //发送用户名到光标所在编辑框
	keybd_event(VK_TAB, 0, 0, 0);  //按下Tab键使光标跳到下一编辑框                        
	keybd_event(VK_TAB, 0, KEYEVENTF_KEYUP, 0);
	keybd_event(VK_TAB, 0, 0, 0);  //按下Tab键使光标跳到下一编辑框                        
	keybd_event(VK_TAB, 0, KEYEVENTF_KEYUP, 0);
	SendKeys(strUserPwd); //发送用户密码到光标所在编辑框   
	
}


void CWindowsToolsDlg::ShowDialog()
{
	if (m_iswindowhide==false)
	{
		this->ShowWindow(SW_SHOW);
		m_iswindowhide = true;
	}
	else
	{
		this->ShowWindow(SW_HIDE);
		m_iswindowhide = false;
	}
}


void CWindowsToolsDlg::OnStnClickedx5()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CWindowsToolsDlg::InputDota()
{
	m_isdota = !m_isdota;
	if (m_isdota == true)
	{
		SetTimer(4,1000,NULL);		
	}
	else
	{
		KillTimer(4);
	}
}


void CWindowsToolsDlg::InputTest()
{
	//HWND hWnd;
	//hWnd = FindWindow(NULL,_T("sandbox"))->m_hWnd;
	//CString str;
	//str.Format(_T("%s"), hWnd);
	//GetDlgItem(IDC_x3)->SetWindowText(str);
	//::PostMessage(hWnd, WM_CLOSE, NULL, NULL);
}

bool iscut = false;
void CWindowsToolsDlg::StopAuto()
{
	if (m_isAutoCreateOn == true)
	{
		keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
		m_isAutoCreateOn = false;
		KillTimer(3);
	}
	if (m_isdota == true)
	{
		KillTimer(4);
		m_isdota = false;
	}
	if (m_isminilevel == true)
	{
		KillTimer(5);
		KillTimer(6);
		KillTimer(7);
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
		m_isminilevel = false;
		KillTimer(MOUSEMOVETIMER_X);
		KillTimer(MOUSEMOVETIMER_Y);
		KillTimer(AUTOCUT);
		iscut= false;
	}
}


void CWindowsToolsDlg::InputMiniLevel()
{
	m_isminilevel = !m_isminilevel;
	if (m_isminilevel == true)
	{		
		SetTimer(5, 100, NULL);
		SetTimer(6, 40000, NULL);
		//POINT point;
		//GetCursorPos(&point); 
		//CString strX, strY;
		//realyx= m_savepoint_x = (int)point.x;
		//realyy= m_savepoint_y = (int)point.y;
		//SetTimer(MOUSEMOVETIMER_X,100,NULL);
		//SetTimer(MOUSEMOVETIMER_Y,2000, NULL);
	}
	else
	{

		KillTimer(5);
		KillTimer(6);
		//KillTimer(MOUSEMOVETIMER_X);
		//KillTimer(MOUSEMOVETIMER_Y);
	}
}
int xxxx = 0, yyyy = 0;
int xxxx1 = 0, yyyy1 = 0;
UINT FuncThread(LPVOID pParam) //参数为CreateThread的第四个参数,此时我传的是一个this指针.
{
	///你的操作.

	Sleep(1000);
	SetCursorPos(xxxx1, yyyy1);
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
	Sleep(200);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
	Sleep(1000);
	SetCursorPos(xxxx1, yyyy1 + 140);
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
	Sleep(100);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(300);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

	int xx = 888, yy = 888;
	SetCursorPos(xxxx, yyyy);
	Sleep(300);
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
	Sleep(200);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
	Sleep(200);
	keybd_event(VK_F5, 0, 0, 0);

	Sleep(3000);
	mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
	Sleep(100);
	mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());


	if (yyyy<540)
		SetCursorPos(xxxx + 42, yyyy + 300);
	else if (yyyy>510)
		SetCursorPos(xxxx + 42, yyyy - 228);


	Sleep(800);
	mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
	Sleep(100);
	mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());

	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

	Sleep(400);
	keybd_event(VK_RETURN, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);





	return 0;
}
void CWindowsToolsDlg::CreateJar()
{

	DWORD dwThreadId = 0;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)FuncThread, this, 0, &dwThreadId);
}
static int countforjar = 0;
void CWindowsToolsDlg::CreateSetMouse()
{
	POINT point;
	GetCursorPos(&point);
	if(countforjar==0)
	{
		yyyy =m_yy = point.y;
		xxxx=m_xx = point.x;
		countforjar = 1;
	}
	else
	{
		yyyy1 = m_yy = point.y;
		xxxx1 = m_xx = point.x;
		countforjar = 0;
	}
	CString str;
	str.Format(_T("(%d,%d)->%d"), xxxx, yyyy,countforjar);
	if(yyyy>540|| xxxx<510)
		GetDlgItem(IDC_x10)->SetWindowText((CString)"位置设定成功"+ str);
	else
		GetDlgItem(IDC_x10)->SetWindowText((CString)"位置设定失败"+ str);

}