
// WindowsToolsDlg.h : 头文件
//

#pragma once


// CWindowsToolsDlg 对话框
class CWindowsToolsDlg : public CDialogEx
{
// 构造
public:
	CWindowsToolsDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WINDOWSTOOLS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedOk();
	void SendUnicode(wchar_t data);
	void SendKeys(CString msg);
	void SendAscii(wchar_t data, BOOL shift);
	afx_msg void OnBnClickedButton2();
	void GetHandle();
	void AntoCreate();
	bool m_isAutoCreateOn;
	int m_loop;
	int m_hwndcount;
	afx_msg void OnBnClickedButton3();
	void InputGit();
	void InputUnity();
	void ShowDialog();
	bool m_iswindowhide;
	afx_msg void OnStnClickedx5();
	int m_istoolopen;
	void InputDota();
	bool m_isdota;
	void InputTest();
	void StopAuto();
	void InputMiniLevel();
	void SessionThread(LPVOID lpvoid);
	void CreateJar();
	void CreateSetMouse();
	bool m_isminilevel;
	int m_savepoint_x;
	int m_savepoint_y;
	int ExportJar;
	int m_xx;
	int m_yy;
};
